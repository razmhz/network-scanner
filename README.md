Basic Network Scanner using Python

Usage:
python network_scanner.py -t {target_ip}

Example:
python network_scanner.py -t 10.0.0.1/24
